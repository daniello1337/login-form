import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <header>
            <div className="header-content">
                <Link to="/"><img src="logo.png" alt="logo" className="logo"/></Link>
                <Link to="/login"><img src="login-logo.png" alt="login-logo" className="login-logo"/></Link>
            </div>
        </header>
    );
};

export default Header;
