import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';
import classNames from 'classnames';
// import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            remember: false,
            emailError: false,
            passwordError: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
    };

    handleChange(event) {
        console.log('change');
        console.log(this.state);
        this.setState({
            [event.target.id]: event.target.value,
            [event.target.id+'Error']: false,
        });
    };

    validateEmail(email) {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    validatePassword(password) {
        let re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/;
        return re.test(password);
    }

    handleSubmit(event) {
        event.preventDefault();
        if (!this.validateEmail(this.state.email)) {
            this.setState({
               emailError: true,
            });
            NotificationManager.error('Invalid email');
            return;
        }

        if (!this.validatePassword(this.state.password)) {
            this.setState({
                passwordError: true,
            });
            NotificationManager.error('Invalid password');
            NotificationManager.warning('Your password needs at least 6 characters, 1 uppercase letter, 1 lowercase letter and  1 number.', null, 10000);
            return;
        }
        NotificationManager.success('Logged in');
        this.props.history.push('/');

        // const url = "http://localhost:3000/api/";
        // const payload = {
        //     "username": this.state.email,
        //     "password":this.state.password
        // };
        // axios.post(url+'login', payload)
        //     .then((response) => {
        //         if(response.data.code == 200) {
        //             NotificationManager.success('Logged in');
        //             this.props.history.push('/');
        //         } else {
        //             NotificationManager.error('Wrong email or password!');
        //         }
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
    };

    render() {
        const emailClass = classNames(
            'email',
            { error: this.state.emailError },
        );
        const passwordClass = classNames(
            'password',
            { error: this.state.passwordError },
        );
        return (
            <div className="form-container">
                <form method="POST" onSubmit={(event) => { this.handleSubmit(event) }}>
                    <fieldset>
                        <h2>Log In</h2>
                        <label htmlFor="email">email</label>
                        <input className={ emailClass } type="text" name="email" id="email" placeholder="email" onChange={(event) => { this.handleChange(event) }} />
                        <label htmlFor="password">password</label>
                        <input className={ passwordClass } type="password" name="password" id="password" placeholder="password" onChange={(event) => { this.handleChange(event) }} />
                        <label htmlFor="remember" className="checkbox-label">Remember me</label>
                        <input type="checkbox" name="remember" id="remember" onChange={() => { this.setState({ remember: !this.state.remember }) }} />
                        <input className="submit-button" type="submit" value="Log in"/>
                    </fieldset>
                </form>
            </div>
        );
    };
};

export default Login;
