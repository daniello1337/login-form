import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Login from './Login';
import Header from './Header';
import './Login.css';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <div>
                        <Header/>
                        <Switch>
                            <Route exact path="/login" component={Login} />
                        </Switch>
                        <NotificationContainer/>
                    </div>
                </Router>
            </div>
    );
  }
}

export default App;
